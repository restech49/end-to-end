package com.restech.users.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PageController {
	
	@RequestMapping("/")
	public String home() {
		return "index";
	}

	@RequestMapping("/home")
	public String home1() {
		return "index";
	}
	
	@RequestMapping("/createusers")
	public String createUsers() {
		return "createusers";
	}
	
	@RequestMapping("/fetchusers")
	public String fetchUsers() {
		return "fetchuser";
	}
	
	@RequestMapping("/searchuser")
	public String searchUsers() {
		return "searchuser";
	}
	
	
}
