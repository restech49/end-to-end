package com.restech.users.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.restech.users.dto.Usersdto;
import com.restech.users.entity.UsersEntity;
import com.restech.users.services.UserServices;

@RestController
public class ApiController {

	@Autowired
	UserServices userServices;
	
	@RequestMapping(value="{userentity}", method=RequestMethod.POST)
	public void saveUsers(@RequestBody UsersEntity userentity) {
		
		userServices.saveUsers(userentity); 
	}
	
	@RequestMapping(value="/fetchusers/{userfirstname}", method=RequestMethod.GET)
	public List<UsersEntity> fetchUsers(@PathVariable String userfirstname) {
		System.out.println("Inside api controller save users "+userfirstname);
		List<UsersEntity> u= userServices.fetchUser(userfirstname); 
		System.out.println("User details"+u);
		return u;
	}
	
	@RequestMapping(value="/deleteusers/{userId}", method=RequestMethod.DELETE)
	public void fetchUsers(@PathVariable Integer userId) {
		System.out.println("Inside api controller save users "+userId);
		userServices.deleteUser(userId); 
	}
}
