package com.restech.users.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.restech.users.dto.Usersdto;
import com.restech.users.entity.UsersEntity;

@Repository
public interface UserRepository extends JpaRepository<UsersEntity, Integer>{
	
	public List<UsersEntity> findByuserAge(Integer age);	 
    public List<UsersEntity> findByuserFirstName(String userFirstName);
}
