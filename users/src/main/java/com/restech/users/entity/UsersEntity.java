package com.restech.users.entity;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
public class UsersEntity implements Serializable{
	
	

	private static final long serialVersionUID = 0x62A6DA99AABDA8A8L;
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Id
	private Integer userId;
	
	@Column
	private String userLastName;
	private String userFirstName;
	private Integer userAge;
	private Integer userPhoneNumber;
	
	
	// will be used to instantiate users entity
		public UsersEntity(Integer userId,String userFirstName, String userLastName, Integer userAge, Integer userPhoneNumber) {
			this.userId=userId;
			this.userLastName = userLastName;
			this.userAge = userAge;
			this.userPhoneNumber = userPhoneNumber;
			this.userFirstName = userFirstName;
		}
		
		// The default constructor required for JPARepository
		protected UsersEntity() {
			
		}

	
	
	public String getUserFirstName() {
		return userFirstName;
	}


	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}


	public String getUserLastName() {
		return userLastName;
	}


	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}


	public Integer getUserAge() {
		return userAge;
	}


	public void setUserAge(Integer userAge) {
		this.userAge = userAge;
	}


	public Integer getUserPhoneNumber() {
		return userPhoneNumber;
	}


	public void setUserPhoneNumber(Integer userPhoneNumber) {
		this.userPhoneNumber = userPhoneNumber;
	}


	public Integer getUserId() {
		return userId;
	}

	
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Override
    public String toString() {
        return String.format("User[id=%d, firstName='%s', lastName='%s', userAge=%d, userPhoneNumber=%d]", userId, userFirstName, userLastName,userAge,userPhoneNumber);
    }

}
