package com.restech.users.services;

import java.util.List;

import com.restech.users.dto.Usersdto;
import com.restech.users.entity.UsersEntity;

public interface UserServices {
	
	void saveUsers(UsersEntity userentity);
	List<UsersEntity> fetchUser(String userFirstName);
	void deleteUser(Integer userId);

}
