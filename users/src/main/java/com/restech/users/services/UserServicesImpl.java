package com.restech.users.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.restech.users.dto.Usersdto;
import com.restech.users.entity.UsersEntity;
import com.restech.users.repository.UserRepository;

@Service
public class UserServicesImpl implements UserServices{

	@Autowired
	UserRepository userRepository;
	
	
	
	@Override
	public void saveUsers(UsersEntity userEntity) {
		
		userRepository.save(userEntity);
		
	}

	@Override
	public List<UsersEntity> fetchUser(String userFirstName) {
		List<UsersEntity> u= userRepository.findByuserFirstName(userFirstName);
		System.out.println("user entity after fetch user" +u);
		return u;
	}
	

	@Override
	public void deleteUser(Integer userId) {
		userRepository.deleteById(userId);
		
	}

}
