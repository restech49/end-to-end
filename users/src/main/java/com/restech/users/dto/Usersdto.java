package com.restech.users.dto;

public class Usersdto {
	
	Integer userId;
	String userFirstName;
	String userLastName;
	Integer userAge;
	Integer userPhoneNumber;
	
	public void Userdto() {
		
	}
	
	public void Usersdto(Integer userId ,String userFirstName,String userLastName,Integer userAge,Integer userPhoneNumber) {
		this.userFirstName=userFirstName;
		this.userLastName=userLastName;
		this.userAge=userAge;
		this.userPhoneNumber=userPhoneNumber;
	}
	

	public Integer getUserId() {
		return userId;
	}


	public void setUserId(Integer userId) {
		this.userId = userId;
	}


	public String getUserFirstName() {
		return userFirstName;
	}


	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}


	public String getUserLastName() {
		return userLastName;
	}


	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}


	public Integer getUserAge() {
		return userAge;
	}


	public void setUserAge(Integer userAge) {
		this.userAge = userAge;
	}


	public Integer getUserPhoneNumber() {
		return userPhoneNumber;
	}


	public void setUserPhoneNumber(Integer userPhoneNumber) {
		this.userPhoneNumber = userPhoneNumber;
	}

}
