'use strict'
angular.module('userapp.services', []).factory('Services', ["$http", "CONSTANTS", function($http, CONSTANTS) {
    var service = {};
    
    service.saveUser = function(users) {
        console.log("save user from services.js")
        console.log(users)
        console.log(CONSTANTS.createUsers)
    	return $http.post(CONSTANTS.createUsers, users);
        
    }
    
    service.fetchUser=function(userFirstName){
    	console.log("inside services fetchuser1")
    	console.log(userFirstName)
    	var url=CONSTANTS.fetchUsers+"/"+userFirstName
    	console.log(url)
    	return $http.get(url)
    }
    
    service.deleteUser = function(userId) {
        console.log("save user from services.js")
        console.log(userId)
        var url=CONSTANTS.deleteUsers + "/" + userId
    	console.log(url)
    	return $http.delete(url);
        
    }
   
    
    return service;
}]);