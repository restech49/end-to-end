'use strict'
var userApp = angular.module('userapp', ['ui.bootstrap', 
                                         'userapp.createUserController',
                                         'userapp.fetchUsersController',
                                         'userapp.deleteUsersController',
                                         'userapp.services']);
console.log("i am inside1 app.js")
userApp.constant("CONSTANTS", {
	createUsers: "/createuser",
	fetchUsers:"/fetchusers",
	deleteUsers:"/deleteusers"
    
});